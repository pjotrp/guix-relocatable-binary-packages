#! /bin/bash
#
# Test with env -i bash --noprofile --norc

if [ "$1" == "--debug" ]; then
    DEBUG=-debug
fi

BIN=$(readlink -f ~/.guix-profile/bin/sambamba)
SAMTOOLS_BIN=$(readlink -f ~/.guix-profile/bin/samtools)
BCFTOOLS_BIN=$(readlink -f ~/.guix-profile/bin/bcftools)
if [ ! -z $DEBUG ]; then
    # add debug info
    GUIXPATH=/gnu/store/hb13hjys1064jmb6z17yc1f822hv9zsz-sambamba-0.6.7-pre1-7cff065
    BIN=$GUIXPATH/bin/sambamba
    SRC=/gnu/store/wzs7nxnsvqy129srpcibs26xf7xsa62b-sambamba-0.6.7-pre1-1f693da-checkout
fi
HASH=$(basename $(dirname $(dirname $BIN)))
CWD=`pwd`

TARBALL=$CWD/$HASH$DEBUG-x86_64.tar
tar cvf $TARBALL -h -C ../gnu-install-bin install.sh installer/
./bin/list-shared-libs $BIN|xargs tar -rvf $TARBALL
./bin/list-shared-libs $SAMTOOLS_BIN|xargs tar -rvf $TARBALL
./bin/list-shared-libs $BCFTOOLS_BIN|xargs tar -rvf $TARBALL

if [ ! -z $DEBUG ]; then
    # This version creates a debug dir, but that is maybe not the best idea - need
    # to test with a working build and check whether the binary contains a route to
    # the debug symbols. Also the sources need to be loaded somehow.
    TDIR=`mktemp --directory`
    # echo $TDIR
    cd $HOME/.guix-profile/lib/debug
    tar rvf $TARBALL -h gnu/store/*sam*/bin/sambamba.debug
    cd $CWD
    # pick up sambamba.debug too
    tar rvf $TARBALL -h $BIN
    tar rvf $TARBALL -h $SRC
fi

if [ -z $DEBUG ]; then
    echo "Compressing..."
    bzip2 -f $TARBALL
fi
