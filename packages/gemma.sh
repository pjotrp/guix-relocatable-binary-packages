#! /bin/bash
#
# Test with env -i bash --noprofile --norc

if [ "$1" == "--debug" ]; then
    DEBUG=-debug
    shift
fi

gemma=$1
if [ -z $gemma ]; then
    gemma=~/.guix-profile/bin/gemma
fi
BIN=$(readlink -f $gemma)
HASH=$(basename $(dirname $(dirname $BIN)))
CWD=`pwd`

TARBALL=$CWD/$HASH$DEBUG-x86_64.tar
tar cvf $TARBALL -h -C ../gnu-install-bin install.sh installer/
./bin/list-shared-libs $BIN|xargs tar -rvf $TARBALL

if [ -z $DEBUG ]; then
    echo "Compressing..."
    bzip2 -f $TARBALL
fi

fn=$(basename $TARBALL.bz2)

cat <<EOF

   1. Download the tarball, e.g.:

        wget $fn

   2. Check the MD5:

        md5sum $fn
        `md5sum $TARBALL.bz2|cut -c 1-32`

   3. Make temporary directory

        mkdir tmp
        cd tmp
        rm -rf gnu/

   4. Unpack the tarball

        tar xvjf ../$fn

   5. Run the installer with a target directory

        ./install.sh ~/gemma

   6. Run the software

        ~/gemma/bin/gemma

EOF
